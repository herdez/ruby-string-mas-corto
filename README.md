# Ejercicio - String más corto

Crea el método `shortest` que recibe como parámetro un `Array` de `Strings` y regresa el string(s) más corto dentro de un arreglo.

Se entregarán dos versiones de este programa, la primera usando `estructura iterativa` y la otra versión usando solamente métodos `enumerables`.


```ruby
#shortest method


#Driver code

p shortest(['uno', 'dos', 'tres', 'cuatro', 'cinco']) == ["uno", "dos"]
p shortest(['gato', 'perro', 'elefante', 'jirafa']) == ["gato"]
p shortest(['verde', 'rojo', 'negro', 'morado']) == ["rojo"]
```